package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

const (
	ansiESC   = "\u001b"
	ansiReset = "[0m"
)

var (
	oldRx, oldTx, txCap, rxCap float64
	run                        = 0
	timer                      time.Time
)

func CString(FgColor int, input string) string {
	return fmt.Sprintf(ansiESC+"[38;5;%dm%s"+ansiESC+ansiReset, FgColor, input)
}

func throughputScaling(input float64) (float64, string) {
	switch {
	case input > 1000000000:
		return input / 1000000000, CString(6, "GB")
	case input > 1000000:
		return input / 1000000, CString(2, "MB")
	case input > 1000:
		return input / 1000, CString(3, "KB")
	case input < 1000:
		return input, CString(1, " B")
	}
	return 0, ""
}

func fileInt(filename string) (float64, error) {
	ba, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Printf("%v", err)
	}
	fo := strings.Replace(string(ba), "\n", "", -1)
	return strconv.ParseFloat(fo, 64)
}

func netStat(netInterface string) {
	log.Printf("time between measurments %s                 ", time.Since(timer))
	timer = time.Now()
	rxFloat, _ := fileInt("/sys/class/net/" + netInterface + "/statistics/rx_bytes")
	txFloat, _ := fileInt("/sys/class/net/" + netInterface + "/statistics/tx_bytes")
	rxChange := rxFloat - oldRx
	scaledRx, scaledRD := throughputScaling(rxChange)
	scaledRxCap, scaledRDCap := throughputScaling(rxCap)
	oldRx = rxFloat
	txChange := txFloat - oldTx
	scaledTx, scaledTD := throughputScaling(txChange)
	scaledTxCap, scaledTDCap := throughputScaling(txCap)
	oldTx = txFloat
	if run < 2 {
		run++
	} else {
		if rxChange > rxCap {
			rxCap = rxChange
		}
		if txChange > txCap {
			txCap = txChange
		}
	}
	floatRes := "3"
	fmt.Printf("RX: %10."+floatRes+"f %s/sec Cap: %10."+floatRes+"f %s     \n",
		scaledRx, scaledRD, scaledRxCap, scaledRDCap)
	fmt.Printf("TX: %10."+floatRes+"f %s/sec Cap: %10."+floatRes+"f %s     \n",
		scaledTx, scaledTD, scaledTxCap, scaledTDCap)
	lineUps := "3"
	fmt.Printf("\033[" + lineUps + "A")

}

func listDir() []os.FileInfo {
	files, err := ioutil.ReadDir("/sys/class/net/")
	if err != nil {
		log.Fatal(err)
	}
	return files
}

func userInputInt() (int, error) {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("interface number: ")
	input, _ := reader.ReadString('\n')
	return strconv.Atoi(strings.Replace(input, "\n", "", -1))
}

func main() {
	timer = time.Now()
	networkInterfaces := listDir()
	for c, v := range networkInterfaces {
		fmt.Println(c, v.Name())
	}
	inputInt, _ := userInputInt()
	oldRx, oldTx = 0, 0
	for {
		go netStat(networkInterfaces[inputInt].Name())
		time.Sleep(time.Second * time.Duration(1))
	}
}